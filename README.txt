        
        
        
        JSpec - JavaScript Behavior Driven Development (BDD) Framework
        Provided by http://vision-media.ca
        Developed by TJ Holowaychuk
        
        ------------------------------------------------------------------------------- 
        INSTALLATION
        ------------------------------------------------------------------------------- 
        
        Enable the module.
        
        ------------------------------------------------------------------------------- 
        PERMISSIONS
        ------------------------------------------------------------------------------- 
        
        administer jspec 
          - allowing viewing and configuration of jspec spec suites.
        
        ------------------------------------------------------------------------------- 
        PUBLIC API
        ------------------------------------------------------------------------------- 
        
        Simple create the directory 'spec' and place your JavaScript spec suites
        in this directory. These files will automatically be retrieved by JSpec.
        
        ------------------------------------------------------------------------------- 
        TODO
        ------------------------------------------------------------------------------- 
        
        - Allow developers to choose which suites run
        - Add feature to run suite within a 'panel' on each page which would allow for
          testing on various pages when suites require elements
        - Add feature for sand-boxes of markup available to the suite(s)