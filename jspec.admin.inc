<?php

/**
 * Run and display reports.
 */
function jspec_reports() {
	$form = array();
  jspec_add_files();
  jspec_add_suites();

	$form['report'] = array(
	    '#type' => 'fieldset',
	    '#title' => t('Report'),
			'#attributes' => array('id' => 'jspec'),
	  );
	
	return $form;
}