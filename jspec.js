
Drupal.behaviors.initJSpec = function(context) {
  if (Drupal.settings.jspec && Drupal.settings.jspec.files)
    for (i = 0, len = Drupal.settings.jspec.files.length; i < len; ++i)
      JSpec.exec(Drupal.settings.jspec.base + Drupal.settings.jspec.files[i])
  JSpec.run().report()
}
